name := """sbt-command-repl"""
organization := "com.glngn"
version := "0.1-SNAPSHOT"

sbtPlugin := true

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % Test

bintrayPackageLabels := Seq("sbt","plugin")
bintrayVcsUrl := Some("""git@gitlab.com:coreyoconnor/sbt-command-repl.git""")

initialCommands in console := """import com.glngn.sbt._"""

// set up 'scripted; sbt plugin for testing sbt plugins
scriptedLaunchOpts ++= Seq("-Xmx1024M", "-Dplugin.version=" + version.value)
