package com.glngn.sbt

import sbt._
import sbt.Keys._
import sbt.plugins.JvmPlugin

object CommandREPLPlugin extends AutoPlugin {
  override def trigger = allRequirements
  override def requires = JvmPlugin

  object autoImport {
  }

  import autoImport._

  override lazy val globalSettings = Seq()
}
